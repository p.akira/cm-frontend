import React from 'react';
import logo from './logo.svg';
import { SearchBar, SearchWrapper } from './components';
import './App.scss';

function App() {
  return (
    <div className="App">
      <header className="App-header shadow py-2 px-5 d-flex flex-row align-items-center justify-content-between">
        <img src={logo} className="App-logo" alt="logo" />
        <SearchBar />
      </header>
      <main>
        <SearchWrapper />
      </main>
    </div>
  );
}

export default App;
