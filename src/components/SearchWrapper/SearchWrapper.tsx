import React, { Component } from 'react'
import { SearchResult } from '../SearchResult'
import './SearchWrapper.scss';

class SearchWrapper extends Component {
  state = {
    searchText: '',
    totalMatches: 200
  }

  render() {
    return (
      <div className="main-content">
        <div className="search-title px-5">
          <h1 className="pl-2 py-3">{this.state.searchText || 'Lista de produtos'}</h1>
        </div>

        <div className="search-content">
          <div className="container">
            <div className="search-results pb-3">
              <h5 className="search-matches py-4 d-inline-block">
                {this.state.totalMatches > 0
                  ? this.state.totalMatches + " PRODUTOS ENCONTRADOS"
                  : "NENHUM PRODUTO ENCONTRADO"
                }
              </h5>
              <SearchResult />
              <SearchResult />
              <SearchResult />
              <SearchResult />
              <SearchResult />
            </div>

            <hr />

            <div className="search-options d-flex flex-row justify-content-between pt-3">
              <select className="page-size-filter btn py-2">
                <option>16 produtos por página</option>
                <option>32 produtos por página</option>
                <option>48 produtos por página</option>
              </select>

              <div className="pagination">
                Paginação aqui
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SearchWrapper