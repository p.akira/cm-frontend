import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import './SearchBar.scss'

interface Props { }

interface State {
  placeholder: string,
  inputValue?: string
}

class SearchBar extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      placeholder: 'Buscar Produtos',
      inputValue: ''
    }

    this.searchTextOnChange = this.searchTextOnChange.bind(this)
  }

  searchTextOnChange(event: any) {
    console.log('get event value', event.target.value)
    this.setState({
      inputValue: event.target.value
    })
  }

  render() {
    return (
      <div className="search-container">
        <span className="pl-4 pr-2">
          <FontAwesomeIcon icon={faSearch} color="#cccccc" />
        </span>
        <span className="pr-4">
          <input
            className="search-input"
            name="searchValue"
            type="search"
            autoComplete="off"
            spellCheck="false"
            onChange={this.searchTextOnChange}
            placeholder={this.state.placeholder}
          />
        </span>
      </div>
    )
  }
}

export default SearchBar;