import React, { Component } from 'react';
import './SearchResult.scss'

interface Props {
  images?: [],
  title?: string,
  shortDesc?: string,
  price?: any,
  discountPrice?: any
}

class SearchResult extends Component<Props, any> {
  constructor(props: Props) {
    super(props)

    this.state = {
      images: [
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png"
      ],
      title: "Kit de cama 210 fios",
      shortDesc: "Classic I - Solteiro Extra",
      price: "R$ 298,00",
      discountPrice: "R$ 98,00"
    }
  }

  render() {
    const strikePriceClass = (this.state.discountPrice) ? 'strike-text' : ''

    return (
      <div className="search-result-wrapper d-flex flex-row flex-start">
        <div className="search-result-images d-inline-block w-50 px-3 py-1">
          <img className="product-image" alt="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png" />
          <img className="product-image" alt="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png" />
          <img className="product-image" alt="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png" />
          <img className="product-image" alt="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/512px-Bootstrap_logo.svg.png" />
        </div>

        <div className="search-result-details d-flex flex-row align-items-center justify-content-between w-100 px-3 py-1">
          <div className="d-flex flex-column justify-content-center">
            <span className="product-name">
              <strong>{this.state.title}</strong>
            </span>
            <span className="product-short-desc">
              {this.state.shortDesc}
            </span>
          </div>

          <div className="d-flex flex-row">
            <span className={`full-price pr-1 ${strikePriceClass}`}>
              {this.state.price}
            </span>
            { this.state.discountPrice &&
              <span className="discount-price">
                por <strong> {this.state.discountPrice} </strong>
              </span>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default SearchResult;